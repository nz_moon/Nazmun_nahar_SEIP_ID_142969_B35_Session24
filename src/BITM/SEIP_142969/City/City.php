<?php

namespace App\City;
use App\Model\Database as DB;

class City extends DB{
    public $id;
    public $name;
    public $city;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo  "I am in City Class";
    }
}