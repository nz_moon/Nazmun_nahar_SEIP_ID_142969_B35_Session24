<?php

namespace App\SummaryOfOrganization;
use App\Model\Database as DB;

class SummaryOfOrganization extends DB{
    public $id;
    public $organization_name;
    public $organization_summary;

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        echo  "I am in Summary Of Organization Class";
    }
}